const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const routes = require('./routes');
const port = 3030;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(routes);

app.use((req, res) => {
   res.send('404 not found');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
